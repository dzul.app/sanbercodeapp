import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Quiz from './quiz_3/index';

const App = () => {
  return <Quiz />;
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
