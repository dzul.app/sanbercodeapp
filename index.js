/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Youtube from './tugas/tugas_12/App';
import Mockup from './tugas/tugas_13/App';
import ToDo from './tugas/tugas_14/App';
import AuthStack from './tugas/tugas_15/index';
import TugasStack from './tugas/tugas_15/TugasNavigation/index';
import Quiz from './quiz_3/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
