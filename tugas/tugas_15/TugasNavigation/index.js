import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import LoginScreen from './Login';
import AboutScreen from './AboutMe';
import SkillScreen from './Skills';
import AddScreen from './Add';
import ProjectScreen from './Project';
import HomeScreen from './Home';

const RootStack = createStackNavigator();
const BottomNavStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

function Root() {
  return (
    <RootStack.Navigator>
      <RootStack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <RootStack.Screen
        name="Home"
        component={Drawer}
        options={{title: 'Root'}}
      />
    </RootStack.Navigator>
  );
}

function Drawer() {
  return (
    <DrawerStack.Navigator>
      <DrawerStack.Screen name="About" component={AboutScreen} />
      <DrawerStack.Screen name="Tabs" component={BottomNav} />
    </DrawerStack.Navigator>
  );
}

function BottomNav() {
  return (
    <BottomNavStack.Navigator>
      <BottomNavStack.Screen name="Skill" component={SkillScreen} />
      <BottomNavStack.Screen name="Project" component={ProjectScreen} />
      <BottomNavStack.Screen name="Add" component={AddScreen} />
    </BottomNavStack.Navigator>
  );
}

export default function index() {
  return (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({});
