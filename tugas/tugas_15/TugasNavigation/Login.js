import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

export default class Login extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.iconWrapper}>
          <Text>Icon or slide show</Text>
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Input email or username</Text>
          <TextInput
            style={styles.textInputField}
            placeholder={'Email or Username'}
          />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Input your password</Text>
          <TextInput style={styles.textInputField} placeholder={'Password'} />
        </View>
        <View style={styles.textInputWrapper}>
          <Text>Input your confirmation password</Text>
          <TextInput
            style={styles.textInputField}
            placeholder={'Confirmation Password'}
          />
        </View>
        <View style={styles.textButtonWrapper}>
          <TouchableOpacity
            style={styles.buttonTouchable}
            onPress={() => this.props.navigation.push('Home')}>
            <Text>Submit</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  iconWrapper: {
    height: 300,
    width: 300,
    backgroundColor: '#F4FAFF',
    marginHorizontal: 50,
    marginVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputWrapper: {
    marginLeft: 30,
    marginBottom: 5,
  },
  textInputField: {
    height: 55,
    width: 340,
    backgroundColor: '#F4FAFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'black',
  },
  buttonTouchable: {
    height: 40,
    width: 140,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F4FAFF',
  },
  textButtonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    // backgroundColor: 'red',
  },
});
