import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Project() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Halaman Project</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
  },
});
