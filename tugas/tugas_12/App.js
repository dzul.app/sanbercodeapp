import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Logo from './assets/images/logo.png';
import Icon from 'react-native-vector-icons/MaterialIcons';
import data from './assets/data.json';
import VideoItem from './components/videoItem';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image source={Logo} style={styles.logoHeader} />
        <View style={styles.rightNavBar}>
          <TouchableOpacity style={styles.navItem}>
            <Icon name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navItem}>
            <Icon name="account-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.body}>
        <FlatList
          data={data.items}
          renderItem={video => <VideoItem video={video.item} />}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={() => (
            <View style={{height: 0.5, backgroundColor: '#E5E5E5'}} />
          )}
        />
      </View>

      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25} />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25} />
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25} />
          <Text style={styles.tabTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25} />
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 55,
    paddingHorizontal: 15,
    elevation: 3,
  },
  logoHeader: {
    width: 96,
    height: 22,
  },
  rightNavBar: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 15,
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4,
  },
});
