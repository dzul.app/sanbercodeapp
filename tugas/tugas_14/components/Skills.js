import React, {Component} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Navbar from '../../tugas_13/components/Navbar';
import SkillItem from './SkillItem';

import dataSkills from '../skillData.json';

export default class Skills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: dataSkills.items,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Navbar title="Skills" />
        <View style={styles.categorySkillsWrapper} />
        <FlatList
          data={this.state.item}
          renderItem={skill => <SkillItem items={skill.item} />}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4FAFF',
    marginBottom: 10,
  },
  categorySkillsWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  textCategory: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
});
