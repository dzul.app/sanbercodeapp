import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';

export default class SkillItem extends Component {
  render() {
    let skill = this.props.items;
    return (
      <View>
        {/* <View style={styles.categorySkillsWrapper}>
          <Text style={styles.textCategory}>{category}</Text>
        </View> */}
        <View style={styles.skillsWrapper}>
          <View style={styles.nameOfSkillsWrapper}>
            <Image style={styles.photoIcon} source={{uri: skill.logoUrl}} />
            <Text style={styles.textLeft}>{skill.skillName}</Text>
          </View>
          <View style={styles.identityRightWrapper}>
            <Text style={styles.textRight}>{skill.percentageProgress}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  skillsWrapper: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
  },
  categorySkillsWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  nameOfSkillsWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  photoIcon: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  identityRightWrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  textCategory: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  textLeft: {
    marginHorizontal: 10,
  },
  textRight: {
    marginHorizontal: 20,
  },
});
