import React, {Component} from 'react';
import {StyleSheet, View, Image, Text, ScrollView} from 'react-native';
import Navbar from './components/Navbar';
import Java from './assets/Java.png';
import Javascript from './assets/Javascript.png';
import Mariadb from './assets/Maria.png';
import Postgresql from './assets/Postgres.png';
import Spring from './assets/Spring.png';
import Angular from './assets/Angular.png';
import ReactNative from './assets/React.png';
import Eclipse from './assets/Eclipse.png';
import Intellij from './assets/Intellij.png';
import Visual from './assets/Visual.png';
import Postman from './assets/Postman.png';

export default class Skills extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Navbar title="Skills" />
          <View style={styles.categorySkillsWrapper}>
            <Text style={styles.textCategory}>Programming Language</Text>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Java} />
              <Text style={styles.textLeft}>Java</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>Intermediate</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Javascript} />
              <Text style={styles.textLeft}>Javascript</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>Intermediate</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Mariadb} />
              <Text style={styles.textLeft}>Mariadb</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>Intermediate</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Postgresql} />
              <Text style={styles.textLeft}>Postgresql</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>Intermediate</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.categorySkillsWrapper}>
            <Text style={styles.textCategory}>Framework</Text>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Spring} />
              <Text style={styles.textLeft}>Spring MVC</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>75%</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Angular} />
              <Text style={styles.textLeft}>AngularJS</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>50%</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={ReactNative} />
              <Text style={styles.textLeft}>React Native</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>30%</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.categorySkillsWrapper}>
            <Text style={styles.textCategory}>Tools</Text>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Eclipse} />
              <Text style={styles.textLeft}>Eclipse IDE</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>80%</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Intellij} />
              <Text style={styles.textLeft}>Intellij Idea</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>70%</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Visual} />
              <Text style={styles.textLeft}>Visual Code Studio</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>70%</Text>
            </View>
          </View>
          <View style={styles.skillsWrapper}>
            <View style={styles.nameOfSkillsWrapper}>
              <Image style={styles.photoIcon} source={Postman} />
              <Text style={styles.textLeft}>Postman</Text>
            </View>
            <View style={styles.identityRightWrapper}>
              <Text style={styles.textRight}>70%</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4FAFF',
    marginBottom: 10,
  },
  photoIcon: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  skillsWrapper: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
  },
  categorySkillsWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  nameOfSkillsWrapper: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  identityRightWrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  textCategory: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  textLeft: {
    marginHorizontal: 10,
  },
  textRight: {
    marginHorizontal: 20,
  },
});
