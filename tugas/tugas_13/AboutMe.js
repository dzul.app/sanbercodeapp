import React, {Component} from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import Navbar from './components/Navbar';
import PhotoProfile from './assets/Normal.jpg';

export default class AboutMe extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Navbar title="About Me" />
        <View style={styles.profileWrapper}>
          <Image style={styles.photoProfile} source={PhotoProfile} />
        </View>
        <View style={styles.identityWrapper}>
          <View style={styles.identityLeftWrapper}>
            <Text style={styles.textLeft}>Nama</Text>
            <Text style={styles.textLeft}>Date of Birth</Text>
            <Text style={styles.textLeft}>Place of Birth</Text>
            <Text style={styles.textLeft}>Facebook</Text>
            <Text style={styles.textLeft}>Gitlab</Text>
            <Text style={styles.textLeft}>Github</Text>
          </View>
          <View style={styles.identityRightWrapper}>
            <Text style={styles.textRight}>Hilmy Dzul Faqar</Text>
            <Text style={styles.textRight}>4 November 1993</Text>
            <Text style={styles.textRight}>Bandung</Text>
            <Text style={styles.textRight}>hilmy.faqar</Text>
            <Text style={styles.textRight}>dzul.app</Text>
            <Text style={styles.textRight}>dzulfaqar93</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4FAFF',
  },
  profileWrapper: {
    height: 180,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  photoProfile: {
    height: 150,
    width: 150,
    borderRadius: 150 / 2,
  },
  identityWrapper: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
  },
  identityLeftWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  identityRightWrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  textLeft: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  textRight: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
});
